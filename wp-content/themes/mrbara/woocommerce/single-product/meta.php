<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $post;
$product_extra = mrbara_theme_option( 'show_product_extra' );
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>
	<?php if ( in_array( 'sku', $product_extra ) ) { ?>
		<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

            <span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'mrbara' ); ?>
                <span class="sku">
                    <?php if ( $sku = $product->get_sku() ) {
	                    echo wp_kses_post( $sku );
                    } else {
	                    esc_html_e( 'N/A', 'mrbara' );
                    } ?>
                </span>
            </span>

		<?php endif; ?>
	<?php } ?>

	<?php if ( in_array( 'categories', $product_extra ) ) {
		if ( function_exists( 'wc_get_product_category_list' ) ) {
			echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in"><label>' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'mrbara' ) . '</label> ', '</span>' );
		} else {
			$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
			$cat_html  = $product->get_categories( '<span>,</span> ', '<span class="posted_in"><label>' . _n( 'Category:', 'Categories:', $cat_count, 'mrbara' ) . '</label> ', '</span>' );
			echo ! empty( $cat_html ) ? $cat_html : '';
		}
	}
	?>

	<?php if ( in_array( 'tags', $product_extra ) ) {
		if ( function_exists( 'wc_get_product_tag_list' ) ) {
			echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as"><label>' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'mrbara' ) . '</label> ', '</span>' );
		} else {
			$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
			$tag_html  = $product->get_tags( '<span>,</span> ', '<span class="tagged_as"><label>' . _n( 'Tag:', 'Tags:', $tag_count, 'mrbara' ) . '</label> ', '</span>' );
			echo ! empty( $tag_html ) ? $tag_html : '';
		}
	} ?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
