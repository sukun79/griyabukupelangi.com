<div class="footer-layout-5">
	<div class="mr-container-fluid">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<?php mrbara_footer_socials(false, false); ?>
			</div>
			<div class="col-md-6 col-sm-6 text-right">
				<?php mrbara_footer_language(); ?>
			</div>
		</div>
	</div>
</div>