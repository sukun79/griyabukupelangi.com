<div class="footer-layout-6">
	<div class="mr-container-fluid">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<?php mrbara_footer_copyright();?>
			</div>
			<div class="col-md-6 col-sm-6 text-right">
				<?php mrbara_footer_socials(); ?>
			</div>
		</div>
	</div>
</div>