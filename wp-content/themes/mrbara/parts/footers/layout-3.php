<div class="footer-layout-3 footer-vertical">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12">
				<?php mrbara_footer_menu(); ?>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 text-center footer-copyright">
				<?php mrbara_footer_copyright(); ?>
				<?php mrbara_footer_logo( get_template_directory_uri() . '/img/logo/logo-5.png', 'text-center' ); ?>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 text-right">
				<?php mrbara_footer_socials(true); ?>
			</div>
		</div>
	</div>
</div>