<div class="container-fluid mr-container-fluid">
	<div class="row">
		<div class="menu-logo col-md-6 col-sm-6">
			<?php get_template_part( 'parts/logo' ); ?>
		</div>
		<div class="menu-extra menu-extra-right text-right col-md-6 col-sm-6">
			<?php mrbara_extra_menu(); ?>
		</div>
		<?php mrbara_icon_menu() ?>

	</div>
</div>
