<div class="container">
	<div class="row">
		<div class="menu-logo col-md-2 col-sm-2 col-xs-2">
			<?php get_template_part( 'parts/logo' ); ?>
		</div>
		<div class="primary-nav nav col-sm-10 col-xs-10 col-md-10 text-right">
			<?php mrbara_header_menu(); ?>
		</div>
		<?php mrbara_icon_menu() ?>
	</div>
</div>