<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package MrBara
 */
?>

<?php do_action( 'mrbara_before_site_content_close' ); ?>

</div><!-- #content -->

<?php do_action( 'mrbara_before_footer' ); ?>

<footer id="colophon" class="site-footer">

	<?php do_action( 'mrbara_footer' ); ?>

</footer><!-- #colophon -->

<?php do_action( 'mrbara_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>
<!-- WhatsHelp.io widget -->

<script type="text/javascript">

    (function () {

        var options = {

            whatsapp: "+628989070003", // WhatsApp number

            call_to_action: "Message us", // Call to action

            position: "right", // Position may be 'right' or 'left'

        };

        var proto = document.location.protocol, host = "getbutton.io", 
url = proto + "//static." + host;

        var s = document.createElement('script'); s.type = 
'text/javascript'; s.async = true; s.src = url + 
'/widget-send-button/js/init.js';

        s.onload = function () { WhWidgetSendButton.init(host, proto, 
options); };

        var x = document.getElementsByTagName('script')[0]; 
x.parentNode.insertBefore(s, x);

    })();

</script>

<!-- /WhatsHelp.io widget -->
</body>
</html>
