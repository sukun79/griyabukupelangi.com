Mr.Bara - Responsive Multi-Purpose eCommerce WordPress Theme
------------------------------

Mr.Bara is a Responsive Multi-Purpose eCommerce WordPress Theme. The theme is WooCommerce Ready and will help you build any kind of online store. This theme is suited for any type of website, e-commerce or personal or business use.


Change log:

Version 1.8.3

- Update WPBakery Page Builder 6.4.2
- Update: Revolution Slider 6.2.23

Version 1.8.2

- Update: Compatibility with WooCommerce 4.4.1
- Update: Revolution Slider 6.2.21

Version 1.8.1

- Update: Compatible with WooCommerce 4.2.0
- Update WPBakery Page Builder 6.2.0
- Update: Revolution Slider 6.2.9

Version 1.8.0

- Update: Compatible with WooCommerce 4.0.0

Version 1.7.9

- Update: Compatible with WooCommerce 3.9.1
- Fix: Some minor bugs.

Version 1.7.8

- Update: Compatible with WooCommerce 3.8

Version 1.7.7

- Update WPBakery Page Builder 6.0.5
- Update: Compatible with WooCommerce 3.7

Version 1.7.6

- Update WPBakery Page Builder 6.0.2

Version 1.7.5

- Update: Compatible with Gutenberg

Version 1.7.4

- Update: Compatible with WooCommerce 3.6.1

Version 1.7.3

- Update: Compatible with WordPress 5.0
- Fix: Some minor bugs.

Version 1.7.2

- Update WPBakery Page Builder 5.6
- Update: Compatible with WooCommerce 3.5.1

Version 1.7.1

- Fix: Wrong text domain

Version 1.7.0

- Update WPBakery Page Builder 5.5.5
- Update: Compatible with WooCommerce 3.5.0
- Fix: Some minor bugs.

Version 1.6.9

- Update WPBakery Page Builder 5.5.4
- Update: Compatible with WooCommerce 3.4.4
- Fix: Some minor bugs.

Version 1.6.8

- Update WPBakery Page Builder 5.5.2
- Update Slider Revolution 5.4.8
- Fix: wrong image popup in the variable product.
- Fix: Some minor bugs.

Version 1.6.7

- Update: Compatible with WooCommerce 3.4.1

Version 1.6.6

- Fix: Duplicate wishlist and compare icons in single product.

Version 1.6.5

- Update: Compatible with WooCommerce 3.4
- Fix: Some minor bugs.

Version 1.6.4

- New: Add a step option for counter element.
- New: Add a columns option for images carousel element
- Fix: Can't get instagram photo in the instagram shortcode
- Fix: Some minor bugs

Version 1.6.3

- Fix: Scroll dropdown in the checkout page
- Fix: Can't get instagram photo in the instagram shortcode
- Fix: Wrong cart remove url
- Fix: Can't change color for the content in single page.

Version 1.6.2

- Update: Compatible with WooCommerce 3.3.3

Version 1.6.1

- Fix: Warning zero columns

Version 1.6.0
- Update: Compatible with WooCommerce 3.3.1
- Fix: Don't show upsells products

Version 1.5.7

- Update: Compatibility with WordPress 4.9
- Update: Visual Composer 5.4.3
- Update: Revolution Slider 5.4.6.3.1

Version 1.5.6

- Fix: Notice error in checkout page.

Version 1.5.5

- Update: Compatible WooCommerce 3.2.1
- Update: Visual Composer 5.4.2
- Update: Revolution Slider 5.4.6.2
- Update: Apply Custom Out Of Stock Text in single product.
- Fix: RTL for Product Tab 2 element.

Version 1.5.4

- New: Add About 4 use WP editor for the description.
- New: Add Image caption for gallery Lightbox.
- Update: Visual Composer 5.3
- Update: Add the link for team shortcode.
- Update: Search form to translate search text in the theme.
- Fix: Can't check Agree to the Terms and Conditions on Mobile.
- Fix: Can't disable my account in the header on Mobile.

Version 1.5.3

- Update: Visual Composer 5.2.1
- New: Add option allow disable the breadcrumb in the product page.

Version 1.5.2

- New: Add options allow disable related products in the single product.
- Update: Compatible WooCommerce 3.1.1.
- Fix: Can't remove item cart using AJAX

Version 1.5.1
- Update: Visual Composer 5.2
- Fix: Some bugs about CSS

Version 1.5.0

- Update: Revolution Slider 5.4.5.1
- Fix: Wrong the term link in checkout page.

Version 1.4.9
- Fix: Wrong the term HTML in checkout page.

Version 1.4.8
- Update: Compatible WooCommerce 3.1.0.

Version 1.4.7
- New: Add post format option allow disable it in single post.
- Fix: Responsive logo when enter the width and the height.
- Fix: Responsive video in product description.
- Fix: Can't touch product image on Mobile.
- Fix: Missing CSS in compare product.

Version 1.4.6
- New: Add new option allow enable AJAX Add To Cart button in Single Product.
- New: Add new option allow disable Add To Cart button in Product Grid Layout.
- Fix: Don't show featured products in some elements/

Version 1.4.5
- Update: Compatible WooCommerce 3.0.7
- Fix: Some bugs about RTL.
- Fix: Product description doesn't work with shortcode

Version 1.4.4
- Update: Compatible WooCommerce 3.0.6
- Fix: Some bugs about RTL.
- Fix: Some bugs about CSS on mobile.

Version 1.4.3
- Fix: Paragraph Formatting description in single product.
- Fix: Product image doesn't change when select a variation.

Version 1.4.2
- Update: Compatible Woothumbs plugin.
- Fix: RTL Product Tab 2

Version 1.4.1
- Update: Revolution Slider 5.4.3.1
- Update: Product Gallery, Product Thumbnails Slider.
- Fix: The description of product category is cut.
- Fix: Some bugs about CSS.

Version 1.4
- Add: columns and category options for team element.
- Fix: error in checkout template.
- Fix: The link of icon cart doesn't work.
- Fix: Some bugs about CSS.

Version 1.3.9
- Update: Compatible WooCommerce 3.0.3
- Fix: Some bugs about CSS.

Version 1.3.8
- Update: Visual Composer 5.1.1
- Update: Compatible WooCommerce 3.0.1
- Fix: Some bugs about RTL.

Version 1.3.7
- New: Add new a footer allow select 6 columns
- Fix: Product Category page doesn't work with shop list layout.
- Fix: Remove Product Zoom in product page layout 7

Version 1.3.6
- New: Add AJAX remove product item in mini cart.
- New: Add a new option to show product category in Appearance > Customize > Woocommerce > Show Category Description
- Update: Visual Composer 5.1
- Update: Revolution Slider 5.4.1
- Fix: Add custom logo for page.

Version 1.3.5
- New: Add new option allow disable images size that don't use in Appearance > Customize > General > Image Sizes
- Fix: Missing some font icons
- Fix: Can't click attribute swatch in product grid layout 10 after loading ajax
- Fix: Wrong portfolio masonry when having many items
- Fix: Can't change product grid layout in single post
- Fix: Some bugs about CSS.

Version 1.3.4
- Fix: Hidden Share Text If disable all social icons in product page.
- Fix: Search products by category.
- Fix: Responsive the content with header left.
- Fix: Show product extra for all product page layouts.
- Fix: Can't disable product zoom in the quickview

Version 1.3.3
- New: Add custom page header for product page.
- Fix: Can't select product category in some elements.
- Fix: Don't show Google Map if enter the width of this element.
- Fix: Some bugs about CSS in Safari.

Version 1.3.2
- New: Add Banners Slider for Category Box 2 element.
- Update: Remove pagination in Product Tabs Style 5 element.
- Fix: Some bugs about CSS.

Version 1.3.1
- New: Support RTL
- New: Add new option to display newsletter after how many seconds.
- New: Add share vk.com to post, product, portfolio

Version 1.3
- New: Add Home Marketplace 3.
- New: Add Home Book 2.
- New: Top Promotion layout 2.
- New: Add vk.com to footer socials.
- Fix: Error sticky header layout 11.
- Fix: Some bugs about CSS.

Version 1.2.2
- New: Add video to product gallery.
- New: Add Carousel for related products.
- New: Add label for image swatches in product page.
- Fix: Error style when click filter attributes in shop page.
- Fix: Error scroll of product layout 4.
- Fix: Some bugs about CSS.

Version 1.2.1
- New: Add custom font in customizer
- New: Add a new option to disable/enable share socials
- New: Show gallery in lightbox for all product page layouts
- New: Add a new option allow assign the link to slider in MrBara Slider element

Version 1.2
- New: Add Home Marketplace 1 demo content.
- New: Add Home Marketplace 2 demo content.
- New: Add Header top layout 12.
- New: Add new style for Categories menu.
- New: Add a new option allow  choose categories menu items is open or close on the homepage.
- New: Add a new option allow show categories menu items is a click or hover.
- New: Add a new option allow display mini cart buttons in multiple lines.
- New: Add Product Category Box element.
- New: Add Product Category Box 2 element.
- New: Add new style for Products Tabs element.
- New: Add new style for Image Carousel element.
- New: Add a new option to show for the excerpt for MrBara Posts element.
- New: Add a new option to show dark skin for MrBara Info Banner element.
- Fix: Show date format of the post by  date format of system.
- Fix: Use Logo Transparent option to show logo in my account layout 2, 404 page.
- Fix: Don't show Settings button to show the mega menu panel.

Version 1.1.3
- New: Add product images carousel for product layout 10
- New: Add sticky content for product layout 3, 7 and 11
- Update: Show content instead of expert in product layout.
- Update: Increase the width and height of logo for all header layouts.
- Fix: Can't search Ajax products by SKU with product variation.

Version 1.1.2
- New: Add Ajax products search by SKU.
- New: Add a new option allow disable secondary product thumbnail when hover over the main product image.
- New: Add options allow change text of search form and product categories in header.
- Update: Increase the width and height of logo in header layout 9
- Fix: Hot deal product element doesn't work with product variation.
- Fix: Hot deal product element can't change currency.

Version 1.1.1
- New: Add a new option allow custom preloader color.
- New: Add a new option allow enter custom javascript.
- New: Add a new option allow disable SKU, categories, tags, share in product page.
- New: Add page header setting for all product page layout.
- Fix: Don't show cross sells product in cart page.
- Fix: Logo doesn't change in stick header layout 10
- Fix: Responsive the top bar on shop page.

Version 1.1
- New: Add Home Tech V4
- New: Add Home Book
- New: Add Header Top Layout 11
- New: Add Promotion on top header
- New: Add Footer Links option allow enter custom link in footer layout 8
- New: Add a new option allow disable product zoom in product page.
- Fix: Can't translate language in backend.

Version 1.0.3
- New: Add new custom badges text for products. Go to the product->Extra->Custom Badges Text
- New: Add a new option in Product Detail element to show a product quick view
- Update: Visual Composer 5.0.1
- Fix: Can't add a new menu.

Version 1.0.2
- New: Add a new option to disable the top bar on mobile
- New: Add new options to change text of badges such as hot, new, sale and out of stock.
- Update: Update color and label Variations swatches for all product page laypouts.
- Fix: Can't change shop layout when set the shop page is homepage.
- Fix: Missing custom layout images in pages.
- Fix: Missing CSS when switch grid and list view and enable categories filter on shop page.
- Fix: Some bugs about CSS

Version 1.0.1
- Update: Add a new option allow to disable responive.
- Update: Add a new option allow to change 2 columns for product page on mobile.
- Fix: Can't change color scheme when use the child theme.
- Fix: Missing custom layout images in pages.
- Fix: Missing CSS product widgets in footer.

Version 1.0
- Initial
