jQuery(document).ready(function ($) {
	"use strict";

	// Show/hide settings for post format when choose post format
	var $format = $('#post-formats-select').find('input.post-format'),
		$formatBox = $('#post-format-settings');

	$format.on('change', function () {
        var type = $(this).filter(':checked').val();
        postFormatSettings(type);
	});
	$format.filter(':checked').trigger('change');


    $(document.body).on('change', '.editor-post-format .components-select-control__input', function () {
        var type = $(this).val();
        postFormatSettings(type);
    });

    $(window).load(function () {
        var $el = $(document.body).find('.editor-post-format .components-select-control__input'),
            type = $el.val();
        postFormatSettings(type);
    });

    function postFormatSettings(type) {
        $formatBox.hide();
        if ($formatBox.find('.rwmb-field').hasClass(type)) {
            $formatBox.show();
        }

        $formatBox.find('.rwmb-field').slideUp();
        $formatBox.find('.' + type).slideDown();
    }

	// Show/hide settings for custom layout settings
	$('#custom_layout').on('change', function () {
		if ($(this).is(':checked')) {
			$('.rwmb-field.custom-layout').slideDown();
		}
		else {
			$('.rwmb-field.custom-layout').slideUp();
		}
	}).trigger('change');

	// Show/hide settings for custom layout settings
	$('#custom_page_header_layout, #custom_product_header_layout').on('change', function () {
		if ($(this).is(':checked')) {
			$('.rwmb-field.page-header-layout').slideDown();
		}
		else {
			$('.rwmb-field.page-header-layout').slideUp();
		}
	}).trigger('change');

	$('#display-settings,#product-display-settings').find('.page-header-layout .rwmb-image-select').css({
		width : '450',
		height: 'auto'
	});



	// Show/hide settings for template settings
	$('#page_template').on('change', function () {

        pageHeaderSettings($(this));

	}).trigger('change');

    $(document.body).on('change', '.editor-page-attributes__template .components-select-control__input', function () {
        pageHeaderSettings($(this));
    });

    $(window).load(function () {
        var $el = $(document.body).find('.editor-page-attributes__template .components-select-control__input');
        pageHeaderSettings($el);
    });

    function pageHeaderSettings($el) {

        if ($el.val() == 'template-homepage.php' ||
            $el.val() == 'template-homepage-transparent.php' ||
            $el.val() == 'template-home-boxed-content.php' ||
            $el.val() == 'template-home-cosmetic.php' ||
            $el.val() == 'template-home-split.php' ||
            $el.val() == 'template-home-width-1620.php') {
            $('#display-settings').hide();
            $('#display-comingsoon').hide();
            $('#display-settings .hide-fullwidth').hide();
        } else if ($el.val() == 'template-full-width.php') {
            $('#display-comingsoon').hide();
            $('#display-settings').show();

            $('#display-settings .hide-fullwidth').hide();

        } else {
            $('#display-settings').show();
            $('#display-settings .hide-fullwidth').show();
            $('#display-comingsoon').hide();

            if ($el.val() == 'template-coming-soon.php') {
                $('#display-comingsoon').show();
                $('#display-settings').hide();
                $('#display-settings .hide-fullwidth').hide();
            }
        }
    };

	$('#display-settings').find('.hide-custom-page-header').hide();

	$('#variable_product_options').on('reload', function (event, data) {
		var postID = $('#post_ID').val();
		$.ajax({
			url     : ajaxurl,
			dataType: 'json',
			method  : 'post',
			data    : {
				action : 'product_meta_fields',
				post_id: postID
			},
			success : function (response) {
				$('#product_attributes_extra').empty().append(response.data);
			}
		});
	});

	// Show/hide settings for custom layout settings
	var custom = false;
	$('#product-display-settings').on('change', '.rwmb-image_select', function () {
		if ($(this).is(':checked') ) {
			if ($(this).val() == '3') {
				custom = true;
			} else {
				custom = false;
			}
		}

		if (custom) {
			$('#product-display-settings').find('.bg-page-header').slideDown();
		}
		else {
			$('#product-display-settings').find('.bg-page-header').slideUp();
		}
	}).trigger('change');

});
