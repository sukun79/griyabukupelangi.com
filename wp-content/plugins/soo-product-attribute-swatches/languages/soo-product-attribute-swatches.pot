# Loco Gettext template
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Soo Product Attribute Swatches\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Mon Aug 01 2016 13:39:17 GMT+0700 (SE Asia Standard Time)\n"
"POT-Revision-Date: Mon Aug 01 2016 13:39:20 GMT+0700 (SE Asia Standard Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#. Name of the plugin
msgid "Soo Product Attribute Swatches"
msgstr ""

#. URI of the plugin
msgid "http://soothemes.com/plugin/wc-product-attribute-swatches"
msgstr ""

#. Description of the plugin
msgid ""
"An extension of WooCommerce to make variation products more beauty and "
"friendly with users."
msgstr ""

#. Author of the plugin
msgid "SooThemes"
msgstr ""

#. Author URI of the plugin
msgid "http://soothemes.com"
msgstr ""

#: /soo-product-attribute-swatches.php:54 /includes/class-admin.php:298
msgid "Color"
msgstr ""

#: /soo-product-attribute-swatches.php:55 /includes/class-admin.php:305
msgid "Image"
msgstr ""

#: /soo-product-attribute-swatches.php:56 /includes/class-admin.php:320
msgid "Label"
msgstr ""

#: /soo-product-attribute-swatches.php:162
msgid ""
"Soo Product Attribute Swatches is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: /includes/class-admin.php:84
msgid "Choose an image"
msgstr ""

#: /includes/class-admin.php:85
msgid "Use image"
msgstr ""

#: /includes/class-admin.php:143 /includes/class-admin.php:311
msgid "Upload/Add image"
msgstr ""

#: /includes/class-admin.php:144 /includes/class-admin.php:312
msgid "Remove image"
msgstr ""

#: /includes/class-admin.php:189
msgid "Select terms"
msgstr ""

#: /includes/class-admin.php:200
msgid "Select all"
msgstr ""

#: /includes/class-admin.php:201
msgid "Select none"
msgstr ""

#: /includes/class-admin.php:202
msgid "Add new"
msgstr ""

#: /includes/class-admin.php:265
msgid "Add new term"
msgstr ""

#: /includes/class-admin.php:269
msgid "Name"
msgstr ""

#: /includes/class-admin.php:275
msgid "Slug"
msgstr ""

#: /includes/class-admin.php:287
msgid "Cancel"
msgstr ""

#: /includes/class-admin.php:288
msgid "Add New"
msgstr ""

#: /includes/class-admin.php:347
msgid "Wrong request"
msgstr ""

#: /includes/class-admin.php:351
msgid "Not enough data"
msgstr ""

#: /includes/class-admin.php:355
msgid "Taxonomy is not exists"
msgstr ""

#: /includes/class-admin.php:359
msgid "This term is exists"
msgstr ""

#: /includes/class-admin.php:373
msgid "Added successfully"
msgstr ""
